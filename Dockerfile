FROM openjdk:15-jdk-alpine
COPY target/apicurso-0.0.1.jar app.jar
EXPOSE 8083
ENTRYPOINT ["java", "-jar", "/app.jar"]
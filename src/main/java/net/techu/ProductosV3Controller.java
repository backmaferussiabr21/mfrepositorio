package net.techu;

import net.techu.data.ProductoMongo;
import net.techu.data.ProductoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.security.PublicKey;
import java.util.ArrayList;
import java.util.List;

//@SpringBootApplication
public class ProductosV3Controller implements CommandLineRunner {

    @Autowired
    private ProductoRepository repository;
    public static void main(String[] args){
        SpringApplication.run(ProductosV3Controller.class, args);
    }
    @Override
    public void run(String... args) throws Exception{
        System.out.println("Preparando MongoDB");
//PARA BORRAR TODOS:
//        repository.deleteAll();
//INSERTAR
//        repository.insert(new ProductoMongo("PR1", "99"));
//        repository.insert(new ProductoMongo("PR2", "89"));
//LEER
        List<ProductoMongo> lista = repository.findAll();
        for (ProductoMongo p:lista) {
            System.out.println(p.toString());
        }
    }

}

package net.techu;
import net.bbva.Calculadora;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController

public class SaludoControl {
    @RequestMapping("/saludo")
    public String index() {
        Calculadora calculadora = new Calculadora();
        double resultado = calculadora.obtenerPrecioFinal(100);
        return "Hola, yo soy tu API y el precio final de 100 es " + String.valueOf(resultado);
    }

}

package com.example.demo;

import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

//@SpringBootTest
@AutoConfigureMockMvc
public class SaludoControlTest {
//    @Test
    public void firstTest() {
        System.out.println("Primer test");
    }
    @Autowired
    private MockMvc mockMvc;
//    @Test
    public void getSaludo() throws Exception {
        mockMvc.perform(MockMvcRequestBuilders.get("/saludo")).andExpect(status().isOk());
    }
}
